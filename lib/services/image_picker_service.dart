// import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:image_picker_for_web/image_picker_for_web.dart';
import 'package:image_picker/image_picker.dart';

class ImagePickerService {
  // Returns a [File] object pointing to the image that was picked.
  Future<PickedFile> pickImage({@required ImageSource source}) async {
    ImagePicker img = ImagePicker();

    return img.getImage(source: source);
  }
}
